<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Photos album upload</title>
<style type="text/css">
#upload-form-border {
	border-style: dashed;
	padding: 10px;
	width: 400px;
}
</style>
<script>
	var validFileExtensions = [ ".pdf" ];
	function validate(form) {
		var arrInputs = form.getElementsByTagName("input");
		for (var i = 0; i < arrInputs.length; i++) {
			var input = arrInputs[i];
			if (input.type == "file") {
				var fileName = input.value;
				if (fileName.length > 0) {
					var isValid = false;
					for (var j = 0; j < validFileExtensions.length; j++) {
						var sCurExtension = validFileExtensions[j];
						if (fileName.substr(
								fileName.length - sCurExtension.length,
								sCurExtension.length).toLowerCase() == sCurExtension
								.toLowerCase()) {
							isValid = true;
							break;
						}
					}
					if (!isValid) {
						alert("Please choose a PDF file! ("
								+ validFileExtensions.join(", ") + ")");
						return false;
					}
				} else {
					alert("Please choose a PDF to upload!");
					return false;
				}
			}
		}

		return true;
	}
</script>
</head>
<body>
	<h1>PDF upload</h1>
	<div id="upload-form-border">
		<form enctype="multipart/form-data" action="upload" method=POST onsubmit="return validate(this);">
			<p><b>Select your PDF file:</b></p> 
			<input name="file" type="file"> 
			<input type="submit" value="Upload">
		</form>
	</div>
	<br>
	<a href="list">View uploaded PDF</a>
</body>
</html>