<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My uploaded PDF files</title>
</head>
<body>
	<h1>Uploaded files</h1>
	<table border="1">
		<tr>
			<td width="200px">
					<b>File name</b>
			</td>
			<td><b>URL</b></td>
		</tr>
		<c:forEach items="${pdfFileList}" var="pdfFile">
			<tr>
				<td width="200px">
					<p>
						<b>${pdfFile.name}</b>
					</p>
				</td>
				<td><p>${pdfFile.url}</p></td>
			</tr>
		</c:forEach>
	</table>
	<br>
	<a href="upload">Upload more PDF files</a>
</body>
</html>