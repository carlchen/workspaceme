package au.com.auspost.iso.secops;




import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CtrlMain extends Application {

	String Maintitle = "Australia Post ISO SecOps Dashboard";
	String icon = "file:icon//ap.png";
	String uname="carl";
	String passw="letmein";
	
	int MainLength = 900;
	int MainWidth = 450;

	public void start(Stage myAdminStage) {

		Group myroota = new Group();

		myAdminStage.setTitle(Maintitle);
		myAdminStage.getIcons().add(new Image(icon));
		myAdminStage.setScene(new Scene(myroota, MainLength, MainWidth,
				Color.DARKGREY));
		// Image stop = new Image("file:images//StopSign1.jpg");
		//ImagePattern pattern = new ImagePattern(stop);
		//myAdminStage.getScene().setFill(pattern);
		
		myAdminStage.getScene().getStylesheets().add("secops.css");

		Canvas canvas = new Canvas(MainLength, MainWidth);

		myroota.getChildren().add(canvas);

		GraphicsContext gc = canvas.getGraphicsContext2D();

		Text scenetitle = new Text("ISO SecOps Trend Mirco Dashboard");
		scenetitle.setId("scenetitleAdmin");
		scenetitle.setX(120);
		scenetitle.setY(120);

		Text welcome = new Text("Welcome");
		welcome.setId("welcome");
		welcome.setX(540);
		welcome.setY(160);

		Label userName = new Label("User Name:");
		userName.setId("userName");
		userName.setLayoutX(180);
		userName.setLayoutY(220);

		TextField userTextField = new TextField();
		userTextField.setId("userTextField");
		userTextField.setLayoutX(300);
		userTextField.setLayoutY(220);

		Label pw = new Label("Password:");
		pw.setId("Password");
		pw.setLayoutX(180);
		pw.setLayoutY(260);

		PasswordField pwBox = new PasswordField();
		pwBox.setId("pwBox ");
		pwBox.setLayoutX(300);
		pwBox.setLayoutY(260);

		Button login = new Button("Login");
		login.setId("login ");
		login.setLayoutX(400);
		login.setLayoutY(300);

		List<Node> list = new ArrayList<Node>();
		
		Label author = new Label("@Australia Post 2017");
		//userName.setId("userName");
		author.setLayoutX(600);
		author.setLayoutY(400);

		list.add(welcome);
		list.add(scenetitle);
		list.add(userName);
		list.add(userTextField);

		list.add(pw);
		list.add(pwBox);
		list.add(login);
		list.add(author);
		
		
		

		myroota.getChildren().addAll(list);

		gc.setFill(Color.RED);

		myAdminStage.show();
		
		login.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				
				if(uname.equals(userTextField.getText()) && passw.equals(pwBox.getText())){
					
					System.out.println("good");
					
					Iterator<Node> namesIterator = list.iterator();
					
					// Traversing elements
					while(namesIterator.hasNext()){
						namesIterator.next().setVisible(false);	
						welcome.setText("welcome "+uname +"!");
						welcome.setVisible(true);
						
					}	
					
				}
				else{
					System.out.println("bad");
					
				}
				
				
			}
		});
		
		
	}

	
}

