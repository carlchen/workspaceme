public class appCtrlEvents {

	String aPPCONTROLEVENTID;
	String lOGDATE;
	String hOSTID;
	String aCRULESETID;
	String oPERATION;
	String bLOCKREASON;
	String pATH;
	String fILENAME;
	String fILESIZE;
	String sHA256;
	String sHA1;
	String mD5;
	String pROCESSNAME;
	String pROCESSID;
	String uSERNAME;
	String uSERID;
	String gROUPNAME;
	String gROUPID;
	String hOSTASSETVALUE;
	String tAGSETID;
	String oRIGIN;
	String dECISIONLOGID;

	public void appCtrlEvents(String APPCONTROLEVENTID, String LOGDATE,
			String HOSTID, String ACRULESETID, String OPERATION,
			String BLOCKREASON, String PATH, String FILENAME, String FILESIZE,
			String SHA256, String SHA1, String MD5, String PROCESSNAME,
			String PROCESSID, String USERNAME, String USERID, String GROUPNAME,
			String GROUPID, String HOSTASSETVALUE, String TAGSETID,
			String ORIGIN, String DECISIONLOGID) {
		this.aPPCONTROLEVENTID = APPCONTROLEVENTID;
		this.lOGDATE = LOGDATE;
		this.hOSTID = HOSTID;
		this.aCRULESETID = ACRULESETID;
		this.oPERATION = OPERATION;
		this.bLOCKREASON = BLOCKREASON;
		this.pATH = PATH;
		this.fILENAME = FILENAME;

	}
	
	public String getAPPCONTROLEVENTID(){
		return aPPCONTROLEVENTID;
	}
	
	public void setAPPCONTROLEVENTID(String APPCONTROLEVENTID) {
		this.aPPCONTROLEVENTID = APPCONTROLEVENTID;
	}

}
