package au.edu.swin.ajs.student.chen;



import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.text.StyledEditorKit;


public class swing {
	
	 private static void createAndShowGUI() {
	        //Create and set up the window.
	        JFrame frame = new JFrame("HelloWorldSwing");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        //add menu bar
	        
	        JMenuBar bar =new JMenuBar();
	        
	        //add menu 
	        
	        JMenu menu = new JMenu("Style");
	        Action action = new StyledEditorKit.BoldAction();
	        action.putValue(Action.NAME, "Bold");
	        
	        // add recursively
	        menu.add(action);
	        bar.add(menu);
	        
	        
	        //Add the ubiquitous "Hello World" label.
	        JLabel label = new JLabel("Hello World");
	        frame.getContentPane().add(label);
	        frame.setJMenuBar(bar);
	        //Display the window.
	        frame.pack();
	        frame.setVisible(true);
	    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		javax.swing.SwingUtilities.invokeLater(
				new Runnable() {
                          public void run() {
                                              createAndShowGUI();
                                             }
                                }
		);
    }



}
