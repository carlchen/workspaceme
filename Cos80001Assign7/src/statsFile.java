import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;


public class statsFile {
	
	String filea;
	String patha;
	
	
	public statsFile(String patha,String filea){
		
		this.filea=filea;
		this.patha=patha;
		
	}
	
	public ArrayList<String> getStat() throws IOException{
		
		ArrayList<String> ma= new ArrayList<>() ;
		
		  long T1 = System.currentTimeMillis();
		
		 Path file = Paths.get(patha,filea);
		 BasicFileAttributes basicAttr = Files.readAttributes(file, BasicFileAttributes.class);
		
		 FileTime creationTime = basicAttr.creationTime();
		 ma.add("CreatingTime: "+creationTime.toString());
		 FileTime lastAccessTime = basicAttr.lastAccessTime();
		 ma.add("LastAccessTime: "+lastAccessTime.toString());
		 
		 long sizef=basicAttr.size();
		 ma.add("FileSize: "+Long.toString(sizef));
		 
		 long T2 = System.currentTimeMillis();
		 
		 Long diff=T2-T1;
		 
		 ma.add("Checking Time: "+Long.toString(diff)+" ms");
		 
		 
		 return(ma);
		 
	}

}
