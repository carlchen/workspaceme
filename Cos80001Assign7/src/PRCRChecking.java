import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/* by Carl Chen
 * Master IT student @ swinbourne
 * student id: 101665196
 * 
 * 
 */

public class PRCRChecking {

	long ET; // expected storage duration
	double RR1; // data reliability requirement
	double P1 = 0.0073; // disk failure rate patterns
	long startTime; // Start time of the algorithm
	double SDS = 0.0;// set of longest storage durations
	BufferedWriter out = null;
	FileWriter fstream1;

	public PRCRChecking(long et, double rr1) throws IOException {
		/*
		 * et, the expected storage duration, and rr1, the reliability
		 * requirement, is taken from commandline args
		 */
		this.ET = et;
		this.RR1 = rr1;

	}

	private void retrnInterval(double failPattern, int round)
			throws IOException {

		fstream1 = new FileWriter("c:\\linux\\prcrl.txt", true);
		out = new BufferedWriter(fstream1);

		double intval = 0.0;// probe checking interval
		long T;
		P1 = failPattern;

		log("round" + round);

		out.write("Disk fail rate  is " + failPattern + "\n");
		out.write("round" + round + "\n");

		double tt = (Math.exp(-P1 * ET) + ET - 1) / ET;

		// log("two replica reliability threshold"+ tt);
		out.write("two replica reliability threshold" + tt + "\n");

		if ((Math.exp(-P1 * ET) + ET - 1) / ET - RR1 < 0) {

			T = System.currentTimeMillis();
			log("interval " + intval);
			startTime = T;

			while (T <= ET + startTime) {
				log("time " + T);
				long hh = ET + startTime;
				log("ET " + ET);
				log("coma " + hh);

				intval = intval + 0.1;
				log("interval " + intval);

				double tty = RR1 * intval - 2 * Math.exp(-P1 * intval)
						+ Math.exp(-2 * P1 * intval);
				log("heheg" + tty);

				if (tty < 0.00001 && tty > 0) {
					out.write("Good, for " + P1 + " I found inteval at "
							+ intval + "\n");
					log("Good, for " + P1 + " I found inteval at " + intval);

					// System.exit(0);
					SDS = intval;

				}

				T = (long) (T + 1);

			}
			// log(";;"+SDS);
			out.write(";;" + SDS + "\n");

		} else {
			log("Only one replica is needed");
			out.write("Only one replica is needed" + "\n");

		}
		out.close();

	}

	public void log(String output) {
		System.out.println(output);
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		double relia;
		long dura;

		ArrayList<String> ma, s3;

		dura = Long.parseLong(args[0]);

		relia = Double.parseDouble(args[1]);

		PRCRChecking me = new PRCRChecking(dura, relia);
		int base = 0;
		for (int j = 0; j < 100; j++) {
			double pp = 0;
			pp = base + j * 0.0001;
			// me.log("Disk failrate  is " + pp);

			me.retrnInterval(pp, j);

		}

		/*
		 * checking localfile
		 */

		statsFile getStats = new statsFile("/home/ec2-user", "A7.jpg");
		me.log("\n\nnow checking local A7.jpg..");
		ma = getStats.getStat();

		for (String elem : ma) {
			me.log(elem);
		}

		/*
		 * checking S3
		 */
		me.log("\n\nnow checking S3 A7.jpg..");

		s3Stat ss3 = new s3Stat();

		s3 = ss3.getstat();

		for (String elem : s3) {
			me.log(elem);
		}

		me.log("done");
	}

}
