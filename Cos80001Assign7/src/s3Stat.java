import java.util.ArrayList;
import java.util.ListIterator;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;


public class s3Stat {
	
	   private static final long serialVersionUID = 1L;	
	   private  AmazonS3 client = (AmazonS3) new getClient().rtClient();
	    
	    public s3Stat() {
			super();
			// TODO Auto-generated constructor stub
		}
	    
	    
	    public ArrayList<String> getstat(){
	    	
	    	ArrayList<String> s3= new ArrayList<>() ;
	    	
	    	long T1 = System.currentTimeMillis();
	    	
	    	  ObjectListing objs = client.listObjects(secretes.BUCKET_NAME);
	    	  ListIterator<S3ObjectSummary> objIt = (objs.getObjectSummaries())
	  				.listIterator();
	  	    
	  	    
	            while (objIt.hasNext()){
	 			
	 			S3ObjectSummary ok = objIt.next();
	 			if(ok.getKey().toString().matches("A7.*")){
	 				
	 				s3.add("Location: "+ok.getBucketName());
	 				s3.add("LastModified: "+ok.getLastModified());
	 				s3.add("Size: "+ok.getSize());
	 				s3.add("Storage: "+ok.getStorageClass());
	 				long T2 = System.currentTimeMillis();
	 				Long diff=T2-T1;
	 				 
	 				s3.add("Checking Time: "+Long.toString(diff)+" ms");
	 			}
	 			
	 			 
	 		}
	    	return s3;
	    }
	  

}
