package application;
	




import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Group root = new Group();
			Scene scene = new Scene(root,800,400);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			VBox holder = new VBox();
			holder.setLayoutX(50);
			holder.setLayoutY(50);
			Label userName = new Label("User Name:");
			//userName.setId("userName");
			userName.setLayoutX(60);
			userName.setLayoutY(60);
			userName.setVisible(true);
			Label removeLabel = new Label("remove mexxzxzxzx!");
			
			 Canvas canvas = new Canvas(300, 250);
		        GraphicsContext gc = canvas.getGraphicsContext2D();
		      
		        root.getChildren().add(canvas);
			
			
			holder.getChildren().add(removeLabel);
			holder.getChildren().remove(removeLabel);
			
			
			//Rectangle gc= new Rectangle(20,20,200,200);
			
			gc.setFill(Color.BLACK);
			gc.setStroke(Color.BLACK);
			gc.translate(50,50);
			gc.save();
			gc.fillRect(0.0, 0.0, 100, 100);
			gc.restore();
			gc.translate(100,100);
			gc.rotate(45.0);
			//gc.translate(50,50);
			gc.scale(0.5, 0.5);
			gc.strokeRect(0.0, 0.0, 100, 100);
			
			
			root.getChildren().addAll(holder,userName);
			
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
