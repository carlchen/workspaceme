package application;

public class CalculateThread extends Thread {
	private int x, y = 2;

	public static void main(String[] args) throws Exception {
		new CalculateThread().calculate();
	}

	public CalculateThread() {
		System.out.println("begin");
		x = 5;
		start();
	}

	public void calculate() throws Exception {
		join();
		x = x - y;
		System.out.println(x);
	}

	public void run() {
		System.out.println("run");
		x *= 2;
		System.out.println(x);
		
	}
}